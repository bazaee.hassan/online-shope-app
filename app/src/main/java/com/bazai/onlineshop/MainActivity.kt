package com.bazai.onlineshop

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.Scaffold
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.unit.LayoutDirection
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.bazai.onlineshop.navigation.BottomNavigationBar
import com.bazai.onlineshop.navigation.SetupNavGraph
import com.bazai.onlineshop.ui.components.AppConfig
import com.bazai.onlineshop.ui.components.ChangeStatusBarColor
import com.bazai.onlineshop.ui.theme.OnlineShopTheme
import com.bazai.onlineshop.util.Constants.ENGLISH_LANGUAGE
import com.bazai.onlineshop.util.Constants.PERSIAN_LANGUAGE
import com.bazai.onlineshop.util.Constants.USER_LANGUAGE
import com.bazai.onlineshop.util.LocaleUtil
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private lateinit var navController: NavHostController

    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            OnlineShopTheme {

                navController = rememberNavController()
                ChangeStatusBarColor(navController)
                AppConfig()

                LocaleUtil.setLocale(LocalContext.current, USER_LANGUAGE)

                val direction = if (USER_LANGUAGE == PERSIAN_LANGUAGE){
                    LayoutDirection.Rtl
                }else{
                    LayoutDirection.Ltr
                }

                CompositionLocalProvider(LocalLayoutDirection provides direction ) {
                    Scaffold(
                        bottomBar = {
                            BottomNavigationBar(
                                navController = navController,
                                onItemClick = {
                                    navController.navigate(it.route)
                                }
                            )

                        }
                    ) {
                        SetupNavGraph(navController = navController)
                    }
                }


            }
        }
    }
}