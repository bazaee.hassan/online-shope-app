package com.bazai.onlineshop.data.model.home

data class MainCategory(
    val _id: String,
    val name: String,
    val image: String
)
