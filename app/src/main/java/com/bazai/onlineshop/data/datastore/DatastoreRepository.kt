package com.bazai.onlineshop.data.datastore

interface DatastoreRepository {
    suspend fun putString(key:String, value:String)
    suspend fun putInteger(key:String, value:Int)

    suspend fun getString(key:String):String?
    suspend fun getInteger(key:String):Int?

}