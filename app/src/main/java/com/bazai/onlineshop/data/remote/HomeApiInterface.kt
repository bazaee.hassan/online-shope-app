package com.bazai.onlineshop.data.remote

import com.bazai.onlineshop.data.model.ResponseResult
import com.bazai.onlineshop.data.model.home.AmazingProduct
import com.bazai.onlineshop.data.model.home.MainCategory
import com.bazai.onlineshop.data.model.home.Slider
import retrofit2.Response
import retrofit2.http.GET
import javax.inject.Inject

interface HomeApiInterface {

    @GET("getSlider")
    suspend fun getSlider() : Response<ResponseResult<List<Slider>>>

    @GET("getAmazingProducts")
    suspend fun getAmazingProducts():Response<ResponseResult<List<AmazingProduct>>>

    @GET("getSuperMarketAmazingProducts")
    suspend fun getSuperMarketAmazingProducts():Response<ResponseResult<List<AmazingProduct>>>

    @GET("get4Banners")
    suspend fun get4Banners():Response<ResponseResult<List<Slider>>>

    @GET("getCategories")
    suspend fun getCategories():Response<ResponseResult<List<MainCategory>>>

    @GET("getCenterBanners")
    suspend fun getCenterBanners():Response<ResponseResult<List<Slider>>>

    @GET("getBestsellerProducts")
    suspend fun getBestsellerProducts():Response<ResponseResult<List<AmazingProduct>>>

    @GET("getMostVisitedProducts")
    suspend fun getMostVisitedProducts():Response<ResponseResult<List<AmazingProduct>>>

    @GET("getMostFavoriteProducts")
    suspend fun getMostFavoriteProducts():Response<ResponseResult<List<AmazingProduct>>>
}