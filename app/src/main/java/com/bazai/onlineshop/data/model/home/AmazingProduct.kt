package com.bazai.onlineshop.data.model.home

data class AmazingProduct(
	val seller: String,
	val image: String,
	val discountPercent: Int,
	val price: Long,
	val name: String,
	val id: String
)

