package com.bazai.onlineshop.data.model.home

data class Slider(
    val image: String,
    val _id: String,
    val category: String,
    val priority: Int,
    val url: String
)
