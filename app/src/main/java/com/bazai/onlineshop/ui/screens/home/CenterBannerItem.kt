package com.bazai.onlineshop.ui.screens.home

import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.hilt.navigation.compose.hiltViewModel
import com.bazai.onlineshop.data.model.home.Slider
import com.bazai.onlineshop.data.remote.NetworkResult
import com.bazai.onlineshop.ui.components.CenterBannerComponent
import com.bazai.onlineshop.viewmodel.HomeViewModel

@Composable
fun CenterBannerItem(
    bannerNumber: Int,
    viewModel: HomeViewModel = hiltViewModel()
) {
    var centerBannerList by remember {
        mutableStateOf<List<Slider>>(emptyList())
    }

    var loading by remember {
        mutableStateOf(false)
    }

    val centerBannerResult by viewModel.centerBanner.collectAsState()
    when (centerBannerResult) {
        is NetworkResult.Success -> {
            centerBannerList = centerBannerResult.data ?: emptyList()
            loading = false
        }

        is NetworkResult.Error -> {
            loading = false
            Log.d("TAG_CenterBannerResult", "CenterBannerItem: ${centerBannerResult.message}")
        }

        is NetworkResult.Loading -> {
            loading = true
        }
    }

    if (centerBannerList.isNotEmpty()){
        CenterBannerComponent(centerBannerList[bannerNumber].image)
    }

}