package com.bazai.onlineshop.ui.screens.home

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import com.bazai.onlineshop.R
import com.bazai.onlineshop.data.model.home.AmazingProduct
import com.bazai.onlineshop.data.remote.NetworkResult
import com.bazai.onlineshop.ui.theme.DigikalaLightGreen
import com.bazai.onlineshop.ui.theme.DigikalaLightRed
import com.bazai.onlineshop.viewmodel.HomeViewModel

@Composable
fun AmazingSupermarketOfferSection(viewModel: HomeViewModel = hiltViewModel()) {
    var amazingProductList by remember {
        mutableStateOf<List<AmazingProduct>>(emptyList())
    }

    var loading by remember {
        mutableStateOf(false)
    }

    val amazingProductResult by viewModel.amazingSupermarketProduct.collectAsState()
    when (amazingProductResult) {
        is NetworkResult.Success -> {
            amazingProductList = amazingProductResult.data ?: emptyList()
        }

        is NetworkResult.Error -> {
            Log.e("TAG_AMAZING_PRODUCT", "AmazingProduct: ${amazingProductResult.message}")
        }

        is NetworkResult.Loading -> {
            loading = true
        }
    }

    Column(
        modifier = Modifier
            .background(MaterialTheme.colors.DigikalaLightGreen)
            .fillMaxWidth()
    ) {
        LazyRow(
            modifier = Modifier
                .background(MaterialTheme.colors.DigikalaLightGreen)
        ) {
            item {
                AmazingOfferCard(R.drawable.amazings, R.drawable.fresh)
            }
            items(amazingProductList) {item ->
                AmazingItem(item)
            }
            item {
                AmazingShowMoreItem()
            }
        }
    }
}