package com.bazai.onlineshop.ui.components

import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.bazai.onlineshop.navigation.Screen
import com.bazai.onlineshop.ui.theme.Purple200
import com.google.accompanist.systemuicontroller.rememberSystemUiController

@Composable
fun ChangeStatusBarColor(navController: NavHostController) {

    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val systemUiController = rememberSystemUiController()

    when(navBackStackEntry?.destination?.route){
        Screen.Splash.route -> {
            systemUiController.setStatusBarColor(color = Purple200)
        }

        else -> {
            systemUiController.setStatusBarColor(color = Color.White)
        }
    }
}