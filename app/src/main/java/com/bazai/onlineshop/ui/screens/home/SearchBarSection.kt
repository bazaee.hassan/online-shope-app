package com.bazai.onlineshop.ui.screens.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.bazai.onlineshop.R
import com.bazai.onlineshop.ui.theme.LocalCornerShape
import com.bazai.onlineshop.ui.theme.LocalElevation
import com.bazai.onlineshop.ui.theme.LocalSpacing
import com.bazai.onlineshop.ui.theme.searchBarBg
import com.bazai.onlineshop.ui.theme.spacing
import com.bazai.onlineshop.ui.theme.unSelectedBottomBar
import com.bazai.onlineshop.util.Constants.PERSIAN_LANGUAGE
import com.bazai.onlineshop.util.Constants.USER_LANGUAGE

@Composable
fun SearchbarSection() {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .height(65.dp)
            .background(Color.White),
        elevation = LocalElevation.current.extraSmall
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(LocalSpacing.current.small)
                .clip(LocalCornerShape.current.medium)
                .background(MaterialTheme.colors.searchBarBg)
        ) {
            SearchContent()
        }
    }
}

@Composable
private fun SearchContent() {
    Row(
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 20.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Start
    ) {
        Icon(
            modifier = Modifier.size(24.dp),
            painter = painterResource(id = R.drawable.search),
            contentDescription = "",
            tint = Color.LightGray
        )
        Text(
            modifier = Modifier.padding(start = 20.dp),
            textAlign = TextAlign.Center,
            color = MaterialTheme.colors.unSelectedBottomBar,
            style = MaterialTheme.typography.h2,
            text = stringResource(id = R.string.search_in),
            fontWeight = FontWeight.Normal
        )
        Image(
            modifier = Modifier
                .width(80.dp)
                .padding(start = 5.dp),
            painter = changeImageByLanguage(),
            contentDescription = ""
        )
    }
}

@Composable
private fun changeImageByLanguage(): Painter {
    val faImagePainter = painterResource(id = R.drawable.digi_red_persian)
    val enImagePainter = painterResource(id = R.drawable.digi_red_english)
    if (USER_LANGUAGE == PERSIAN_LANGUAGE) {
        return faImagePainter
    }
    return enImagePainter
}