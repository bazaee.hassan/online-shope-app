package com.bazai.onlineshop.ui.screens.home

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavHostController
import com.bazai.onlineshop.R
import com.bazai.onlineshop.navigation.Screen
import com.bazai.onlineshop.ui.components.RoundedIconBox
import com.bazai.onlineshop.ui.theme.LocalSpacing
import com.bazai.onlineshop.ui.theme.amber
import com.bazai.onlineshop.ui.theme.grayAlpha
import com.bazai.onlineshop.ui.theme.grayCategory
import com.bazai.onlineshop.util.Constants

@Composable
fun ShowcaseSection(navController: NavHostController) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = LocalSpacing.current.semiMedium,
                vertical = LocalSpacing.current.biggerSmall
            )
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(LocalSpacing.current.semiSmall),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            RoundedIconBox(
                title = stringResource(id = R.string.digikala_jet),
                image = painterResource(id = R.drawable.digijet),
                onClick = onBoxClick(navController = navController, url = Constants.DIGIJET_URL)
            )

            RoundedIconBox(
                title = stringResource(id = R.string.digi_style),
                image = painterResource(id = R.drawable.auction),
                onClick = onBoxClick(navController = navController, url = Constants.AUCTION_URL)
            )

            RoundedIconBox(
                title = stringResource(id = R.string.digi_pay),
                image = painterResource(id = R.drawable.digipay),
                onClick = onBoxClick(navController = navController, url = Constants.DIGIPAY_URL)
            )

            RoundedIconBox(
                title = stringResource(id = R.string.pindo),
                image = painterResource(id = R.drawable.pindo),
                bgColor = MaterialTheme.colors.amber,
                onClick = onBoxClick(navController = navController, url = Constants.PINDO_URL)
            )
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(LocalSpacing.current.semiSmall),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            RoundedIconBox(
                title = stringResource(id = R.string.digi_shopping),
                image = painterResource(id = R.drawable.shopping),
                onClick = onBoxClick(navController = navController, url = Constants.SHOPPING_URL)
            )

            RoundedIconBox(
                title = stringResource(id = R.string.gift_card),
                image = painterResource(id = R.drawable.giftcard),
                onClick = onBoxClick(navController = navController, url = Constants.GIFT_CARD_URL)
            )

            RoundedIconBox(
                title = stringResource(id = R.string.digi_plus),
                image = painterResource(id = R.drawable.digiplus),
                onClick = onBoxClick(navController = navController, url = Constants.DIGIPLUS_URL)
            )

            RoundedIconBox(
                title = stringResource(id = R.string.more),
                image = painterResource(id = R.drawable.more),
                bgColor = MaterialTheme.colors.grayCategory,
                onClick = onBoxClick(navController = navController, url = Constants.MORE_URL)
            )
        }
    }
}

@Composable
fun onBoxClick(navController: NavHostController, url: String): () -> Unit {
    return {
        navController.navigate(route = Screen.WebView.route + "?url=${url}")
    }
}
