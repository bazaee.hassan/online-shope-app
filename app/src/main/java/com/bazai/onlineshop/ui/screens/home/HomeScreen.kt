package com.bazai.onlineshop.ui.screens.home

import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.bazai.onlineshop.util.Constants
import com.bazai.onlineshop.util.LocaleUtil
import com.bazai.onlineshop.viewmodel.HomeViewModel
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import kotlinx.coroutines.launch

@Composable
fun HomeScreen(navController: NavHostController) {
    LocaleUtil.setLocale(LocalContext.current, Constants.USER_LANGUAGE)
    if (isSystemInDarkTheme()) {
        HomeDarkMode(navController)
    } else {
        HomeLightMode(navController)
    }
}

@Composable
fun HomeLightMode(navController: NavHostController, viewModel: HomeViewModel = hiltViewModel()) {

    LaunchedEffect(true) {
        refreshDataFromServer(viewModel)
    }

    SwipeRefreshSection(viewModel, navController)

}

@Composable
fun HomeDarkMode(navController: NavHostController) {

}

@Composable
fun SwipeRefreshSection(viewModel: HomeViewModel, navController: NavHostController) {
    val refreshScope = rememberCoroutineScope()
    val swipeRefreshState = rememberSwipeRefreshState(isRefreshing = false)
    SwipeRefresh(
        state = swipeRefreshState,
        onRefresh = {
            refreshScope.launch {
                refreshDataFromServer(viewModel)
            }
        }
    ) {
        LazyColumn(
            modifier = Modifier
                .background(Color.White)
                .fillMaxSize()
                .padding(bottom = 60.dp)
        ) {
            item {
                SearchbarSection()
            }

            item {
                TopSlider()
            }

            item {
                ShowcaseSection(navController)
            }

            item {
                AmazingOfferSection(viewModel)
            }

            item {
                ProposalCardSection()
            }

            item {
                AmazingSupermarketOfferSection(viewModel)
            }

            item {
                CategoryListSection()
            }

            item {
                CenterBannerItem(0)
            }

            item {
                BestsellerProducts()
            }

            item {
                CenterBannerItem(1)
            }

            item {
                MostVisitedProducts()
            }

            item {
                CenterBannerItem(2)
            }

            item {
                MostFavoriteProducts()
            }

            item {
                CenterBannerItem(3)
            }

            item {
                CenterBannerItem(4)
            }

        }
    }
}

private suspend fun refreshDataFromServer(viewModel: HomeViewModel) {
    viewModel.getAllDataFromServer()
}