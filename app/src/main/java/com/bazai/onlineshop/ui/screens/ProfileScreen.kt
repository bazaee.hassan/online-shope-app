package com.bazai.onlineshop.ui.screens

import android.app.Activity
import android.content.Intent
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.bazai.onlineshop.MainActivity
import com.bazai.onlineshop.util.Constants.ENGLISH_LANGUAGE
import com.bazai.onlineshop.util.Constants.PERSIAN_LANGUAGE
import com.bazai.onlineshop.viewmodel.DatastoreViewModel

@Composable
fun ProfileScreen(navController: NavHostController, datastore:DatastoreViewModel = hiltViewModel()) {
    if (isSystemInDarkTheme()){
        ProfileDarkMode(navController, datastore)
    }else{
        ProfileLightModel(navController, datastore)
    }
}


@Composable
fun ProfileLightModel(navController: NavHostController, datastore:DatastoreViewModel = hiltViewModel()) {
    val activity = LocalContext.current as Activity

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Button(
            onClick = {
                datastore.saveUserLanguage(PERSIAN_LANGUAGE)
                activity.apply {
                    finish()
                    startActivity(Intent(activity, MainActivity::class.java))
                }
            }
        ) {
            Text(text = "فارسی")
        }

        Button(
            onClick = {
                datastore.saveUserLanguage(ENGLISH_LANGUAGE)
                activity.apply {
                    finish()
                    startActivity(Intent(activity, MainActivity::class.java))
                }
            }
        ) {
            Text(text = "English")
        }
    }
}
@Composable
fun ProfileDarkMode(navController: NavHostController, datastore:DatastoreViewModel = hiltViewModel()) {

}