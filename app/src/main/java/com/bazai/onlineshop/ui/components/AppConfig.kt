package com.bazai.onlineshop.ui.components

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import com.bazai.onlineshop.util.Constants.USER_LANGUAGE
import com.bazai.onlineshop.viewmodel.DatastoreViewModel

@Composable
fun AppConfig(
    datastore: DatastoreViewModel = hiltViewModel()
) {
    getDatastoreVariables(datastore)
}


private fun getDatastoreVariables(datastore: DatastoreViewModel) {
    USER_LANGUAGE = datastore.getUserLanguage()
    datastore.saveUserLanguage(USER_LANGUAGE)
}