package com.bazai.onlineshop.ui.screens.home

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.rememberAsyncImagePainter
import com.bazai.onlineshop.data.model.home.Slider
import com.bazai.onlineshop.data.remote.NetworkResult
import com.bazai.onlineshop.ui.theme.roundedShape
import com.bazai.onlineshop.ui.theme.spacing
import com.bazai.onlineshop.viewmodel.HomeViewModel

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun ProposalCardSection(viewModel: HomeViewModel = hiltViewModel()) {

    var fourBannersList by remember {
        mutableStateOf<List<Slider>>(emptyList())
    }

    var loading by remember {
        mutableStateOf(false)
    }

    val fourBannersResult by viewModel.fourBanners.collectAsState()
    when (fourBannersResult) {
        is NetworkResult.Success -> {
            fourBannersList = fourBannersResult.data ?: emptyList()
        }

        is NetworkResult.Error -> {
            Log.e("TAG_FOUR_BANNERS", "4Banners: ${fourBannersResult.message}")
        }

        is NetworkResult.Loading -> {
            loading = true
        }
    }

    // Compose Design Section
    FlowRow(
        maxItemsInEachRow = 2,
        modifier = Modifier
            .fillMaxWidth()
            .height(290.dp)
            .padding(MaterialTheme.spacing.small)
    ) {
        for (item in fourBannersList){
            ProposalCardItem(item)
        }
    }

}

@Composable
fun ProposalCardItem(imgLink: Slider) {

    Card(
        shape = MaterialTheme.roundedShape.semiMedium,
        modifier = Modifier
            .fillMaxWidth(0.5f)
            .height(140.dp)
            .padding(MaterialTheme.spacing.small)
    ) {
        Image(
            painter = rememberAsyncImagePainter(model = imgLink.image),
            contentDescription = null,
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillBounds
        )
    }

}