package com.bazai.onlineshop.ui.theme

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.ui.unit.dp


data class RoundedShapes(
    val extraSmall: RoundedCornerShape = RoundedCornerShape(4.dp),
    val small: RoundedCornerShape = RoundedCornerShape(8.dp),
    val medium: RoundedCornerShape = RoundedCornerShape(10.dp),
    val semiMedium: RoundedCornerShape = RoundedCornerShape(14.dp),
    val extraMedium: RoundedCornerShape = RoundedCornerShape(16.dp),
    val biggerMedium: RoundedCornerShape = RoundedCornerShape(18.dp),
    val large: RoundedCornerShape = RoundedCornerShape(24.dp),
)

val LocalCornerShape = compositionLocalOf { RoundedShapes() }

val MaterialTheme.roundedShape: RoundedShapes
    @Composable
    @ReadOnlyComposable
    get() = LocalCornerShape.current
