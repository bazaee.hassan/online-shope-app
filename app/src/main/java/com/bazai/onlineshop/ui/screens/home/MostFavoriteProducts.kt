package com.bazai.onlineshop.ui.screens.home

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.hilt.navigation.compose.hiltViewModel
import com.bazai.onlineshop.R
import com.bazai.onlineshop.data.model.home.AmazingProduct
import com.bazai.onlineshop.data.remote.NetworkResult
import com.bazai.onlineshop.ui.theme.DarkCyan
import com.bazai.onlineshop.ui.theme.darkText
import com.bazai.onlineshop.ui.theme.spacing
import com.bazai.onlineshop.viewmodel.HomeViewModel

@Composable
fun MostFavoriteProducts(viewModel: HomeViewModel = hiltViewModel()) {
    var mostFavoriteProductsList by remember {
        mutableStateOf<List<AmazingProduct>>(emptyList())
    }

    var loading by remember {
        mutableStateOf(false)
    }

    val mostFavoriteProductsResult by viewModel.mostFavoriteProducts.collectAsState()

    when (mostFavoriteProductsResult) {
        is NetworkResult.Success -> {
            mostFavoriteProductsList = mostFavoriteProductsResult.data ?: emptyList()
            loading = false
        }

        is NetworkResult.Error -> {
            loading = false
            Log.e("TAG_MOST_FAV", "CategoryListSection: ${mostFavoriteProductsResult.message}")
        }

        is NetworkResult.Loading -> {
            loading = true
        }
    }

    // UI Design
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(MaterialTheme.spacing.small)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(MaterialTheme.spacing.extraSmall),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = stringResource(id = R.string.favorite_product),
                textAlign = TextAlign.Start,
                style = MaterialTheme.typography.h3,
                fontWeight = FontWeight.SemiBold,
                color = MaterialTheme.colors.darkText
            )

            Text(
                text = stringResource(id = R.string.see_all),
                textAlign = TextAlign.End,
                style = MaterialTheme.typography.h6,
                fontWeight = FontWeight.SemiBold,
                color = MaterialTheme.colors.DarkCyan
            )


        }

        LazyRow {
            items(mostFavoriteProductsList){item->
                MostFavoriteProductsOffer(item)
            }
            item {
                MostFavoriteProductsShowMore()
            }
        }
    }
}