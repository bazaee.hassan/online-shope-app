package com.bazai.onlineshop.ui.screens.home

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyHorizontalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.bazai.onlineshop.R
import com.bazai.onlineshop.data.model.home.AmazingProduct
import com.bazai.onlineshop.data.remote.NetworkResult
import com.bazai.onlineshop.ui.theme.darkText
import com.bazai.onlineshop.ui.theme.spacing
import com.bazai.onlineshop.util.DigitHelper
import com.bazai.onlineshop.viewmodel.HomeViewModel

@Composable
fun MostVisitedProducts( viewModel:HomeViewModel = hiltViewModel()) {
    var mostVisitedProductsList by remember {
        mutableStateOf<List<AmazingProduct>>(emptyList())
    }

    var loading by remember {
        mutableStateOf(false)
    }

    val mostVisitedProductsResult by viewModel.mostVisitedProducts.collectAsState()

    when (mostVisitedProductsResult) {
        is NetworkResult.Success -> {
            mostVisitedProductsList = mostVisitedProductsResult.data ?: emptyList()
            loading = false
        }

        is NetworkResult.Error -> {
            loading = false
            Log.e("TAG_MOST_VISITED", "CategoryListSection: ${mostVisitedProductsResult.message}")
        }

        is NetworkResult.Loading -> {
            loading = true
        }
    }

    // Ui design
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(MaterialTheme.spacing.small)
    )
    {
        Text(
            text = stringResource(id = R.string.best_selling_products),
            modifier = Modifier.fillMaxWidth(),
            textAlign = TextAlign.Start,
            style = MaterialTheme.typography.h3,
            fontWeight = FontWeight.SemiBold,
            color = MaterialTheme.colors.darkText
        )

        LazyHorizontalGrid(
            rows = GridCells.Fixed(3),
            modifier = Modifier
                .padding(top = MaterialTheme.spacing.medium)
                .height(250.dp),
            horizontalArrangement = Arrangement.spacedBy(4.dp),
            verticalArrangement = Arrangement.spacedBy(4.dp)
        ) {
            itemsIndexed(mostVisitedProductsList) { index, item ->
                ProductHorizontalCard(
                    name = item.name,
                    id = DigitHelper.digitByLocate((index + 1).toString()),
                    imageUrl = item.image
                )
            }
        }
    }

}