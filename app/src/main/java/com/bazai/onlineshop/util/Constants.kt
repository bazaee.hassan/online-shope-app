package com.bazai.onlineshop.util

import com.bazai.onlineshop.BuildConfig

object Constants {
    const val ENGLISH_LANGUAGE = "en"
    const val PERSIAN_LANGUAGE = "fa"
    const val DATASTORE_NAME = "online_shop_datastore"
    const val TIMEOUT_SECOND:Long = 60

    const val API_KEY = BuildConfig.X_API_KEY

    const val BASE_URL = "https://dig-za0p.onrender.com/api/v1/"

    var USER_LANGUAGE = "USER_LANGUAGE"

    const val DIGIJET_URL = "https://www.digikalajet.com/user/address"
    const val AUCTION_URL = "https://www.digistyle.com/sale-landing/?utm_source=digikala&utm_medium=circle_badge&utm_campaign=style&promo_name=style&promo_position=circle_badge"
    const val DIGIPAY_URL = "https://www.mydigipay.com/"
    const val PINDO_URL = "https://www.pindo.ir/?utm_source=digikala&utm_medium=circle_badge&utm_campaign=pindo&promo_name=pindo&promo_position=circle_badge"
    const val SHOPPING_URL = "https://www.digikala.com/landings/village-businesses/?promo_name=boomi-landing&promo_position=circle_badge"
    const val GIFT_CARD_URL = "https://www.digikala.com/landing/gift-card-landing/?promo_name=gift_landing&promo_position=circle_badge"
    const val DIGIPLUS_URL = "https://www.digikala.com/plus/landing/"
    const val MORE_URL = "https://www.digikala.com/mehr/"

}