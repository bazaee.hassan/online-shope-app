package com.bazai.onlineshop.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bazai.onlineshop.data.model.home.AmazingProduct
import com.bazai.onlineshop.data.model.home.MainCategory
import com.bazai.onlineshop.data.model.home.Slider
import com.bazai.onlineshop.data.remote.NetworkResult
import com.bazai.onlineshop.repository.HomeRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val repository: HomeRepository):ViewModel() {

    val slider = MutableStateFlow<NetworkResult<List<Slider>>>(NetworkResult.Loading())
    val category = MutableStateFlow<NetworkResult<List<MainCategory>>>(NetworkResult.Loading())
    val fourBanners = MutableStateFlow<NetworkResult<List<Slider>>>(NetworkResult.Loading())
    val bestsellerProducts = MutableStateFlow<NetworkResult<List<AmazingProduct>>>(NetworkResult.Loading())
    val mostVisitedProducts = MutableStateFlow<NetworkResult<List<AmazingProduct>>>(NetworkResult.Loading())
    val mostFavoriteProducts = MutableStateFlow<NetworkResult<List<AmazingProduct>>>(NetworkResult.Loading())
    val amazingProduct = MutableStateFlow<NetworkResult<List<AmazingProduct>>>(NetworkResult.Loading())
    val amazingSupermarketProduct = MutableStateFlow<NetworkResult<List<AmazingProduct>>>(NetworkResult.Loading())
    val centerBanner = MutableStateFlow<NetworkResult<List<Slider>>>(NetworkResult.Loading())

    suspend fun getAllDataFromServer(){
        viewModelScope.launch {

            // Fire and Forget
            launch { slider.emit(repository.getSlider()) }

            launch { amazingProduct.emit(repository.getAmazingProducts()) }

            launch { amazingSupermarketProduct.emit(repository.getSuperMarketAmazingProducts()) }

            launch { fourBanners.emit(repository.get4Banners()) }

            launch { category.emit(repository.getCategories()) }

            launch { centerBanner.emit(repository.getCenterBanners()) }

            launch { bestsellerProducts.emit(repository.getBestsellerProducts()) }

            launch { bestsellerProducts.emit(repository.getMostVisitedProducts()) }

            launch { mostFavoriteProducts.emit(repository.getMostFavoriteProducts()) }
        }
    }
}