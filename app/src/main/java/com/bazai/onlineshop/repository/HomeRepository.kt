package com.bazai.onlineshop.repository

import com.bazai.onlineshop.data.model.home.AmazingProduct
import com.bazai.onlineshop.data.model.home.MainCategory
import com.bazai.onlineshop.data.model.home.Slider
import com.bazai.onlineshop.data.remote.BaseApiResponse
import com.bazai.onlineshop.data.remote.HomeApiInterface
import com.bazai.onlineshop.data.remote.NetworkResult
import javax.inject.Inject

class HomeRepository @Inject constructor(private val api: HomeApiInterface) : BaseApiResponse() {

    suspend fun getSlider(): NetworkResult<List<Slider>> {
        return safeApiCall {
            api.getSlider()
        }
    }

    suspend fun getAmazingProducts(): NetworkResult<List<AmazingProduct>> {
        return safeApiCall {
            api.getAmazingProducts()
        }
    }

    suspend fun getSuperMarketAmazingProducts(): NetworkResult<List<AmazingProduct>> {
        return safeApiCall {
            api.getSuperMarketAmazingProducts()
        }
    }

    suspend fun get4Banners(): NetworkResult<List<Slider>> {
        return safeApiCall {
            api.get4Banners()
        }
    }

    suspend fun getCategories(): NetworkResult<List<MainCategory>> {
        return safeApiCall {
            api.getCategories()
        }
    }

    suspend fun getCenterBanners(): NetworkResult<List<Slider>> {
        return safeApiCall {
            api.getCenterBanners()
        }
    }

    suspend fun getBestsellerProducts(): NetworkResult<List<AmazingProduct>> {
        return safeApiCall {
            api.getBestsellerProducts()
        }
    }

    suspend fun getMostVisitedProducts():NetworkResult<List<AmazingProduct>>{
        return safeApiCall {
            api.getMostVisitedProducts()
        }
    }

    suspend fun getMostFavoriteProducts():NetworkResult<List<AmazingProduct>>{
        return safeApiCall {
            api.getMostFavoriteProducts()
        }
    }


}